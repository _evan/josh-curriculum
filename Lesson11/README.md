```bash

heroku pg:backups:capture DATABASE_URL --app model-wiz-server

heroku pg:backups:schedule DATABASE_URL --at '02:00 America/Los_Angeles' --app model-wiz-server

heroku pg:backups:schedules --app model-wiz-server

heroku pg:backups --app model-wiz-server

```


```js
async componentDidMount(){
    window.GC.Spread.LicenseKey='...'
    let workbook = new window.GC.Spread.Sheets.Workbook(this.spreadJs, {sheetCount: 3});

    var excelIO = new window.GC.Spread.Excel.IO();

    const r = await fetch('/files/Headcount Template.xlsx')
    const blob = await r.blob()

    excelIO.open(blob, function(json) {
      workbook.fromJSON(json);
    }, function (e) {
        // process error
        console.log(e);
    });
    //exp
}
```