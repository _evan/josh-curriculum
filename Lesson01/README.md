
# Lesson 1

This week will be an introduction to website deployment and architecture. There are many great web hosting services out there, a couple of my favorites are zeit.co and firebase. Please create a free account on [zeit.co](zeit.co) so we can save time during our lesson

### Agenda

- DNS and hosting
    - static hosting vs dynamic (and why static is so much better)
    - static website libraries, from Bootstrap to React
    - DNS: CNAME records, A records, TXT records
- Deployment
    - zeit / now
    - What is SSL
- website architecture

### Suggested reading:

I love React.js, but maybe you should hear it from someone else as well :) Here is an article outlining the key reasons that React.js has gained massive momentum over the past couple years. It is a bit long, just read the beginning to get an overview: https://medium.freecodecamp.org/yes-react-is-taking-over-front-end-development-the-question-is-why-40837af8ab76

### Homework

Push a "coming soon" page to modelwiz.com

### Deployment and website architecture

In the **modelwiz-landing** folder you will find a very simple site that you can use as a  starting point for a ModelWiz landing page. It uses the Bootstrap CSS library, the template was downloaded from here [https://startbootstrap.com/themes/creative](https://startbootstrap.com/themes/creative)

This week we will learn how to deploy this site for free using [zeit.co](zeit.co), and point your domain name **modelwiz.com** to the deployed site. Of course, feel free to find another template or even start an HTML page from scratch if you want to

### steps to deploy on zeit.co

```bash
# download the now CLI (command-line interface)
npm install -g now
```
on the [zeit.co](zeit.co) website:

- add a domain
- create a DNS TXT record with your registrar with name of **_now** and the provided value
- click "verify domain"
- add a CNAME record pointing to **alias.zeit.co**

```bash
# to deploy:
now
```
on the [zeit.co](zeit.co) website:

- create an **alias** pointing the zeit deployment to your domain name
- create an SSL certificate for your domain name, so your site is secure