# Lesson04

### Prereqs

- Definitely try to figure out how to push/pull from bitbucket :)
- If you want to, check out some other React component libraries, here is a list of some good ones: [https://hackernoon.com/23-best-react-ui-component-libraries-and-frameworks-250a81b2ac42](https://hackernoon.com/23-best-react-ui-component-libraries-and-frameworks-250a81b2ac42). I still think Blueprint looks absolutely amazing, but maybe there are others out there I haven't seen yet.
- Here is a good article on `state`... it might be a bit high-level, but it contains some of the things we will be talking about today: [https://medium.freecodecamp.org/get-pro-with-react-setstate-in-10-minutes-d38251d1c781](https://medium.freecodecamp.org/get-pro-with-react-setstate-in-10-minutes-d38251d1c781)

### Lets build the frontend form

There are a million amazing component libraries to use with React. At my last job we used React-Bootstrap. At my current job we use Antdesign. I have been wanting to try out [Blueprint](https://blueprintjs.com/docs/#blueprint) for a while, so maybe we can start with that. To add it to your project:
```bash
npm i --save @blueprintjs/core
```

Also, you will need to import the CSS for Blueprint, which can be added via CDN directly into your `index.html` file:
```html
<link href="https://unpkg.com/normalize.css@^7.0.0" rel="stylesheet" />
<link href="https://unpkg.com/@blueprintjs/icons@^3.4.0/lib/css/blueprint-icons.css" rel="stylesheet" />
<link href="https://unpkg.com/@blueprintjs/core@^3.10.0/lib/css/blueprint.css" rel="stylesheet" />
```

Now you can start playing around with all the awesome Blueprint components! I think it might work great for Modelwiz because it has a lot of nice components for tables and forms. After you have installed Blueprint, you can import its components like this:
```js
import { InputGroup, Button, Divider } from "@blueprintjs/core";
```

In our main page, we can create a form for submitting the user's email address
```js
<form onSubmit={this.onSubmit}>
  <InputGroup leftIcon="user" placeholder="Email"
    style={{width:300}}  
  />
  <Button icon="small-tick" intent="success" 
    type="submit" text="Notify Me" 
    style={{width:300, marginTop:15}}
  />
</form>
```

### React State

`state` is the most important and awesome feature of React. When you change the `state` of a React component, it will update the DOM for you, without you needing to manually change anything. Under the hood, React holds a "virtual DOM" in memory, and `state` changes are sent to the correct DOM nodes in a very efficient and intelligent way. This allows you to make lots of changes to `state` without needing to worry about re-rendering too much at once.

**The golden rule of `state`**: Never update `state` directly! Updating `state` (like `this.state={n:1}`) will break React's virtual DOM algorithm.

**How to work with `state`**

Initializing `state`
```js
// This is the only time you can directly set state={}
class C extends Component {
  constructor(){
    super()
    this.state={
      value:'',
      something:42,
    }
  }
}
```

Setting `state`. The object that you pass in to `this.setState({})` is *merged* with other values that are in state. So following the example above, `something` will still equal 42, even though you did not pass `something` into `this.setState({})`
```js
// you usually will be setting state based on user interactions, like clicking or typing
this.setState({value:'hello'})
```

Getting `state`.
```js
const value = this.state.value
//or
const {value} = this.state
```

*So now lets add some state to our form!*

- `email` to hold the current value of the email input
- `loading` to show the progress of form submission

### Connecting the the backend (Node.js)

And for actually submitting the form to our backend, we will use the browser's native `fetch` function.

```js
onSubmit = async (e) => {
  e.preventDefault()
  // check email is valid here
  // start loading here
  try {
    const r = await fetch('http://localhost:5000/contact', {
      method: "POST",
      data: JSON.stringify({email:this.state.email})
    })
    const res = await r.json()
    // stop loading here, set text input back to blank
    // show success notification
  } catch(e) {
    // stop loading here
    // show error notification
  }
}
```

Some notes on the code above:

- e.preventDefault() is an important function that prevents the browser from reloading the page on a form submission. In the past, every time you submitted the form, you would have to reload the whole page. Now we can submit forms in the background over AJAX, directly to our Node server.
- again, we are using an `async` function. This allows us to use the `await` keyword in our function. You may have heard of promises before... `await` is the upgraded version of a promise that allows you to write asynchronous code without .then() or callbacks. The code will literally wait at the `await` keyword until that operation is finished.
- after `fetch` you always have to run `r.json()` to get the json response
- `try{}` `catch(){}` is the standard way of handling errors when you are using `await`. Any javascript error inside the `try{}` block will be thrown intro the `catch(){}` block as the argument, so you can then decide what to do with the error there.

### Showing notifications

Blueprint has a nice little notification component that we can use to show users if the form submission was successful or not.

So for example we can show an error if the email is not valid:
```js
// here is a function to validate email using a regex (Regular Expression)
// you dont need to understand what this actually means! I certainly dont
function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
// and here is the code to initialize Blueprint's notification component
// remember to add "Toaster" to the components you are importing from Blueprint
export const Alerts = Toaster.create({
  position: 'top',
})
```

Then, inside your `onSubmit` function, you can show error or success messages like this:
```js
Alerts.show({
  message: "Error creating contact",
  intent: "danger",
})
```

### Back on our server...

Let's check to see that we were successful in adding a contact record to our Postgres database!

```bash
heroku pg:psql
```
```sql
select * from contacts;
```

Congrats! You have created your first full stack application!

### Deploying React to Zeit

If we have time, we can try to build and deploy our application to zeit. This is made very easy by `now` (once we have it set up). The following configurations I got from [https://teamtreehouse.com/library/deploy-to-now](https://teamtreehouse.com/library/deploy-to-now)

now.json:
```json
{
  "version": 2,
  "name": "modelwiz",
  "builds": [
    {
      "src": "package.json", 
      "use": "@now/static-build", 
      "config": {
        "useBuildUtils": "@now/build-utils@canary",
      }
    }
  ],
  "routes": [
    {"src": "^/static/(.*)", "dest": "/static/$1"},
    {"src": "^/favicon.png", "dest": "/favicon.png"},
    {"src": "^/asset-manifest.json", "dest": "/asset-manifest.json"},
    {"src": "^/manifest.json", "dest": "/manifest.json"},
    {"src": "^/service-worker.js", "headers": {"cache-control": "s-maxage=0"}, "dest": "/service-worker.js"},
    {"src": "^/precache-manifest.(.*)", "dest": "/precache-manifest.$1"},
    {"src": "^/(.*)", "dest": "/index.html"}
  ]
}
```

in package.json, we need to add the `now-build` script as well:
```json
{
  "scripts": {
    "now-build": "react-scripts build && mv build dist"
  }
}
```


### Homework

- Spend some time styling the "Coming Soon" page how you like it! Maybe add the cool ModelWiz icon that you showed me as well :)
- Think about what we should add next to the application. My next step would be to build a user authentication system for login and signup, using JWT.
- Here is the Excel API [https://docs.microsoft.com/en-us/office/dev/add-ins/reference/overview/excel-add-ins-reference-overview](https://docs.microsoft.com/en-us/office/dev/add-ins/reference/overview/excel-add-ins-reference-overview). Looks pretty complicated, but not impossible. The crazy success of software-as-a-service startups makes me think that web-based interfaces are always best though :) I would be interested in learning more about your goals for ModelWiz, and what you imaging the business model to be

