# Lesson07

Authorization and Oauth2 are pretty complicated... that is why many companies use services like [Auth0](https://auth0.com/) to handle things for them. At my last company I worked for, we used Auth0, but over time it became really complex to try and customize it. Even though it will take us some time to get Oauth2 set up, we should go for it! By doing more custom work now, it will be easier to integrate other oauth flows, like Quickbooks, in the future.

### Readings

- We will be using a very nice and high-quality library for doing Google Oauth2 with Node/Express. Here is a [blog post](https://medium.com/@bogna.ka/integrating-google-oauth-with-express-application-f8a4a2cf3fee) covering how to get it set up. Before our lesson, see if you can head over to the [Google Developers](https://console.developers.google.com/apis/dashboard) page and register your application (the first section of the blog post covers how to do this.)
- Today we will start to build user authentication in Node.js. We will use JSON Web Tokens (JWT), here is a quick introduction: [https://auth0.com/docs/jwt](https://auth0.com/docs/jwt)

### Making the backend code for user authentication

We will need two routes: `signup` and `login`. So in our Node.js code, lets add those routes. Since they are asking for data from the user (username and password), they will both be POST routes.

To make our `users` table, lets open a Postgres commandline with `heroku pg:psql`
```sql
CREATE TABLE users (
  id bigserial NOT NULL primary key,
  username TEXT,
  encrypted_password TEXT,
  created_time TIMESTAMPZ,
  provider TEXT
);
```

So what do we need to do in order to create a user in our database?

- The new user fills out a form in React
- post the username/password to Node.js
- check if that username already exists
- hash the password
- save the username/hashed password combo as a new record in Postgres
- return a JWT to the user, which authorizes for future calls to Node

```js
// in db.js, lets make a couple new functions
createUser: async (username,passwordHash,time) => pool.query(`INSERT INTO users(username,encrypted_password,created_time) VALUES($1,$2,$3)`, [username,passwordHash,time]),

getUserByName: async (username) => pool.query(`SELECT * from users WHERE username=$1`, [username]),
```
```js
// you need to make a secret key for your JWTs
// export JWT_SECRET=***

// at the top of the file, you will need to import two more libraries
// jsowebtoken
// bcrypt-nodejs

app.post('/signup', async (req, res, next) => {
  console.log('POST /signup')

  const user = await db.getUserByEmail(req.body.username)
  if(user.rowCount){
    return res.status(409).json('User Already Exists')
  }

  let hash
  try {
    hash = bcrypt.hashSync(req.body.password);
  } catch(e) {
    return res.status(500).json('Invalid Password')
  }

  await db.createUser(req.body.username, hash)
  const createdUser = await db.getUserByName(req.body.username)

  const r = createdUser.rows[0]
  const JWTToken = jwt.sign({
    username: r.username,
    id: r.id
  }, process.env.JWT_SECRET, {expiresIn: '72h'})
  
  return res.status(200).json({
     token: JWTToken,
     username: r.username
  })
})
```

Can you make the login route?

### OAuth2

[This post](https://medium.com/@bogna.ka/integrating-google-oauth-with-express-application-f8a4a2cf3fee) looks really great, lets just follow along and see if we can get it working! Instead of using Cookies though, I think integrating JWTs will be better :)

