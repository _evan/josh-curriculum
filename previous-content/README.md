# Curriculum

Here is what I have mapped out for the next couple weeks. This may change as we go along, since there's no real end date for our time, we can go in depth in each of these layers as you like.

## Week 1

We will spend this time getting your environment set up, and getting acquainted with the tools you'll be using, as outlined below.

### Agenda

- Get acquainted with the command line
- Setup Development Environment
  - brew
  - git
  - node
  - VScode
- Transitioning to ES6
- Reviewing Codepen Work

## Week 2

### Data Structures

In this time frame, we're going to be focusing on some basic data structures you will commonly see. I will also be giving you some challenges to finish, to test your understanding on them. I'll also write some tests to evaluate if you've completed the challenge, and considered the possible edge cases for your program.

If possible, I'd like to spend some of our time on the weekend watching you code through a problem that is a little more difficult. Please don't stress about it, it's not an exam. One thing I'd like to discuss is when facing a difficult problem, how to break it down and reason about in small sizes.

### Agenda

- Primitives, eg. Numbers, Strings, Booleans
- Basic Operators
- Arrays and Hashes
- Control Flows (if, if-else, switch)
- Loops
- Functions

## Week 3

### RESTful API's, HTTP

Before we start building our API, we should discuss how some really nice API's are designed. An example of a great API, with extremely clear documentation is the [Stripe API](https://stripe.com/docs/api). It will be helpful not to read through the entirety of the documentation, but take note of the URL's called, and try and recognize the pattern and overall structure. The idea behind this API, and the predominant prevailing thought behind constructing API's can be found in [Roy Thomas Fielding](https://en.wikipedia.org/wiki/Roy_Fielding)'s dissertation, which can be found [here](https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm). This is not required reading, it assumes a knowledge you don't have yet, but good to have as reference.

We'll also make a simple program to call an API endpoint, receive data back, and begin displaying client side.

### Agenda

- Making asynchronous fetch calls to receive JSONs
- Displaying JSON information

## Week 4

### Databases

In this curriculum, we will be using Postgres.

In your applications you've built, you may have noticed that when you exit your browser, the user is reset. We'll discuss how to store user data, and persist it across the web. We'll be discussing How a database works, and how to construct a database in a clear format. We'll also be discussing the relationships between data, and modeling your application in the database.

### Agenda

- Install Postgresql
- Create a database
- Make a database table called "Authors", and relate this table to "Books"

## Week 5

### Let's Start Building an App

At this point, with the above knowledge, you actually know everything you need to start building an application. You won't know all the nuances and security assurances that go into building one, but we'll tackle those as we go. For this week, let's focus on simply creating an app where users can sign in, and log out.

### Agenda

- Begin learning how to make an endpoint
- Make a POST to the endpoint
- Persist the sent User Data
