const title = document.getElementById('todos');


fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => {
    return response.json();
  })
  .then(toDoList => {
    toDoList.forEach(toDoItem => {
      const listItem = document.createElement("LI");
      listItem.className = "list-item";
      const toDoItemTitle = document.createTextNode(toDoItem.title)
      const toDoItemStatus = toDoItem.completed;

      const checkbox = document.createElement('input')
      checkbox.setAttribute("type","checkbox")
      checkbox.checked = toDoItemStatus;

      listItem.appendChild(toDoItemTitle)
      listItem.appendChild(checkbox)

      document.getElementById("toDoItems").appendChild(listItem)
    })

  });

// 1. Fetch data
// 2. Render loading state
// 3. Assign each header the value from fetched data

/*
1) Create an unordered list
2) Interate through the json object that is returned
3) For each object, create a new list item

TODO: Todos List

I'd like to each of the todos in the returned array, in a nice, composable manner.
What I mean by that is, it shouldn't take too much finangling to render these.

https://www.webcomponents.org/introduction

Here's an intro to web components, but you don't have to use that. It's neat, but outside
of our scope. 

Maybe, simply use append child.

So

const todoContainer = document.createElement("li");
const todoTitle = document.createElement('h1');
const todoCompleted = document.createElement('input');
todoCompleted.checked = todo.status

todoContainer.appendChild(todoTitle);
todoContainer.appendChild(todoCompleted);


Here's a rough wireframe of what I'd like to see. It really
doesn't matter what the styling is, I trust you can figure out
CSS. This is much more about javascript, and manipulating the
DOM.

  ____________________________________________________
|                                                      |
|  To Do's:                                            |
|  __________________________________________________  |
| |                                                  | |
| | TODO_TITLE                                    X  | |
| |__________________________________________________| |
|                                                      |
|  __________________________________________________  |
| |                                                  | |
| | TODO_TITLE                                    X  | |
| |__________________________________________________| |
|                                                      |
|  __________________________________________________  |
| |                                                  | |
| | TODO_TITLE                                    X  | |
| |__________________________________________________| |
|                                                      |
|                                                      |
|                                                      |
|                                                      |
|                                                      |
  ____________________________________________________
*/
