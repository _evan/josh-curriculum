# Week 02

## REST api's

This week, we're going to begin moving outside of strictly our local environment, and begin grabbing things from the web via http requests to some RESTful API's.

Some quick terminology first. I'm going to link some good resources for this, and won't write too much. I know it feels like you might not be getting your money's worth by simply linking guides, but I really can't explain better than a lot of people already have.

### API

So, instead of assuming you know what I mean by API, I'll quickly try and explain what it is in simple terms.

An API (Application Programming Interface) is a human readable layer that stands in front of a system, presents programmers with options, and begins doing complex work on behalf of your request.

On your computer, when you click "Save" on a file the developer working on VS Code probably didn't know all of the internals and what it took to actually replace the state of the file with the current one. Instead, whatever operating system they were writing the application for, provided some sort of function called "save," and the developer probably said something akin to:

```
if (clickSave) {
  windows.Save()
}
```

Developers don't have to understand every piece of everything they work on. No one can possibly understand all of Windows, and then start writing on top of it. Instead, developers leave nice packages for one another, to say

"Our project can do these things, don't bother knowing how, but here's the menu."

The same goes for web development. When you make a call to facebook's API, that call will probably, on facebook's server, begin a massive chain of data querying, analytics, security, authentication, and more analytics, before returning what you requested. API's allow us to not worry about what atrocities are going on behind its doors, and rely on facebook's team to keep things smooth and dependable.

[Here](https://medium.freecodecamp.org/what-is-an-api-in-english-please-b880a3214a82) is a decent link going more in depth, you can skip on down to `WWW and remote servers`, (I'd link it but their stupid banner is too big).

It's important to note that when making an API, know that the API _is the product, not a feature_. Thus, API's should be built with consumers in mind. Thus, it _must_ be clean, sensibly organized, and well documented.

### REST

This is a big topic, and I will try and give you the quick gist, and link a website at the end with a lot of in depth information.

REST, short for Representational State Transfer, is a design theory for API's. I cannot go and download REST. It is a theory on how to keep API's clean and easy to use, with clear [separation of concerns](https://effectivesoftwaredesign.com/2012/02/05/separation-of-concerns/), and scalable.

#### Think in Resources

When making an http request to a REST api, think of it in terms of resource instead of action.

Instead of `example.com/api/getUsers?id=1`, think `example.com/api/users/1`.

Everything in a REST api is organized through these resources. In general, there should never be a verb in your url. It should always be a noun. How the API acts on the resource will be determined by your client's [http request](https://www.codecademy.com/articles/http-requests).

#### Make Responses meaningful

Responses from your server should have a status code returned. You shouldn't have to dig through a returned object to see if something succeeded. This way, the front-end can easily know if an error occurred simply based on the returned code.

- `200` - Success.
- `201` - Created. Returned on successful creation of a new resource. Include a 'Location' header with a link to the newly-created resource.
- `400` - Bad request. Data issues such as invalid JSON, etc.
- `404` - Not found. Resource not found on GET.
- `409` - Conflict. Duplicate data or invalid data state would occur.

#### HTTP Methods

These are the methods available through http. You'll have to remember these by heart. There's also OPTIONS, which is purely informational, and returns what methods are available, but we probably won't use this much.

These methods match very closely with `CRUD` (`Create`, `Retrieve`, `Update`, `Delete`) operations. Through these methods, an API will understand how to act on the resource being called.

- `POST` - `Create` and other non-idempotent operations.
- `GET` - `Read` a resource or collection.
- `PUT` - `Update` a resource.
- `DELETE` - `Delete` a resource or collection.

#### CRUD

`CRUD` are the four basic methods for persistent storage. An API should be able to provide these four methods for users to interact with the database, without giving them actual access to the database.

[Here](https://www.codecademy.com/articles/what-is-crud) is a good article on this, with some questions and answers provided. http and CRUD are so interlinked it's hard to write beyond this, but this article is pretty thorough.

#### Idempotent

Idempotent is a fancy word that basically says the behavior is consistent. Your endpoints should always return the same result if you pass the same parameters. You'll see this word thrown around. There are some exceptions, like above, `POST` is the http method that implies a change, so its behavior is not idempotent.

#### Some Helpful Reading

- https://restfulapi.net/
  - [This section](https://restfulapi.net/resource-naming) will give you a good idea of how RESTful API's name and organize their routes.
-

### Promises and Asynchronous Promises

Single Page Applications depend on asynchronous calls. Whenever you see a loading bar, the web page is fetching data, much like ajax.

Nowadays, ajax has gone the way of the dinosaur. JavaScript has a built in method for fetching data now called `fetch`. Fetch returns a promise or a failure

```js
const examplePromise = new Promise((resolve, reject) => {
  setTimeout(function() {
    resolve('foo');
  }, 1000);
});

examplePromise().then(value => console.log(value));
```

In the above piece of code, we create a promise that, in one second, it will return the string `foo`. So, when we call it, after the promise resolves, it will pass the string `foo` as an argument to the function given to the `then` function.

Promises also can fail, and have built it error handling.

```js
const examplePromise = new Promise((resolve, reject) => {
  setTimeout(function() {
    reject('error message');
  }, 1000);
});

examplePromise()
  .then(value => console.log(value))
  .catch(errorMessage => console.log(errorMessage));
```

This above function will do the same thing as the function above, _but_ if it fails, we've added some fallback functionality so that should the Promise fail, we have a way of knowing where, and hopefully, the error message has some explanation why.

I am going to link a [chapter](https://eloquentjavascript.net/11_async.html) of an excellent book called [Eloquent JavaScript](https://eloquentjavascript.net), which is freely available. I prefer not linking a chapter to a book, but this is an _excellent_ book, and this chapter in particular really cleared up a lot of the concept for me.
