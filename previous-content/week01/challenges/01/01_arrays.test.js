import { anagrams, reverseStrings } from './01_arrays';

describe('anagrams', () => {
  xit('returns an array of anagram match arrays', () => {
    const value = anagrams(['abc', 'cab', 'bca', 'sam', 'mat', 'tam']);
    const expectedValue = [['abc', 'cab', 'bca'], ['mat', 'tam']];

    expect(value).toEqual(expectedValue);
  });
});

describe('reverseStrings', () => {
  it('returns an array with reversed strings', () => {
    const value = reverseStrings(['me']);
    const expectedValue = ['em'];

    expect(value).toEqual(expectedValue);
  });
});
