// Prompt: Given an array of strings as a parameter, return an array of the strings
// that match as possible anagrams. If no match, return an empty array.

/*
anagrams(['act', 'cat', 'fat', 'sat', 'spot', 'tops', 'pots'])  =>
[
  [act, cat],
  ['spot', 'tops', 'pots'],
]
*/

export function anagrams(words) {
  let mirrorArray = words;
  let sortedArray = [];
  let finalArray = [];
  let matches = [];
  for (let i = 0; i < words.length; i++) {
    sortedArray.push(
      words[i]
        .split('')
        .sort()
        .join('')
    );
  }
  for (let i = 0; i < sortedArray.length; i++) {
    for (let k = 0; k < sortedArray.length; k++) {
      if (i !== k && i !== '') {
        if (sortedArray[i] === sortedArray[k]) {
          matches.push(mirrorArray[k]);
          sortedArray.splice(k, 1);
          mirrorArray.splice(k, 1);
        }
      }
    }
    if (matches.length > 0) {
      matches.unshift(mirrorArray[i]);
      finalArray.push(matches);
    }

    matches = [];
  }

  return finalArray;
}

function alphabetize(word) {
  word = word.split('');

  return word.sort().join('');
}

function refactoredAnagrams(words) {
  const anagrams = {};
  const returnArray = [];

  words.forEach(word => {
    anagrams[alphabetize(word)] = anagrams[alphabetize(word)]
      ? anagrams[alphabetize(word)].concat(word)
      : [word];
  });

  Object.keys(anagrams).forEach(sortedWord => {
    if (anagrams[sortedWord].length > 1) returnArray.push(anagrams[sortedWord]);
  });

  return returnArray;
}

// Prompt: Given an array of strings, return an array with each of the strings reversed
// and lowercased.

/*
reverseStrings(['Nap', 'light']) => ['pan', 'thgil']
*/

export function reverseStrings(words) {
  const matches = [];
  for (let i = 0; i < words.length; i++) {
    matches.push(
      words[i]
        .split('')
        .reverse()
        .join('')
        .toLowerCase()
    );
  }
  return matches;
}
