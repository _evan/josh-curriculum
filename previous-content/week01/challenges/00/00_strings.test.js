import { interpolate } from './00_strings';

describe('interpolate', () => {
  it('returns string with passed name', () => {
    const expectedValue = 'Hello, Lucas!';
    const value = interpolate('Lucas');

    expect(value).toEqual(expectedValue);
  });

  it('returns string with default name "stranger" when no argument is passed', () => {
    const expectedValue = 'Hello, stranger!';
    const value = interpolate();

    expect(value).toEqual(expectedValue);
  });
});
