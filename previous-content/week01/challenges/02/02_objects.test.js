import { sortAge, sortName } from './02_objects';

const lucas = {
  name: 'Lucas',
  age: 28,
};

const jason = {
  name: 'Jason',
  age: 31,
};

const blake = {
  name: 'Blake',
  age: 27,
};

const melissa = {
  name: 'Melissa',
  age: 29,
};

const madelyn = {
  name: 'Madelyn',
  age: 26,
};

describe(sortAge, () => {
  const people = [lucas, jason, blake, melissa, madelyn];

  it('returns a sorted array by field age', () => {
    const value = sortAge([blake, jason, melissa, madelyn, lucas]);
    const expectedValue = [madelyn, blake, lucas, melissa, jason];

    expect(value).toEqual(expectedValue);
  });
});
