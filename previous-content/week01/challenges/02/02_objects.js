// Prompt: Given an array of objects containing fields name:string, and age:number,
// return an array of the objects sorted. Please write two functions, each sorted by
// the different properties.

/*
const mindy = { name: 'Mindy', age: 28 };
const jack = { name: 'Jack', age: 29 };
const people = [mindy, jack];

sortName(people) => [ jack, mindy ]
sortAge(people) => [ mindy, jack ]
*/

const sortAge = (people) => {
    const ages = [];
    ObjBySortedAges = [];
    namesBySortedAges = [];

    people.forEach(person => ages.push(person.age))

    ages.forEach( age => {
        ObjBySortedAges.push(people.filter(person => person.age === age)[0]);
    });
       
    ObjBySortedAges.forEach(person => {
        namesBySortedAges.push(person.name)
    })

    return namesBySortedAges     
}

const sortName = (people) => {
    const names = [];
    
    people.forEach(person => {
        names.push(person.name)
    })
    names.sort();
    
    return names
}

export function sortName(people) {}

export function sortAge(people) {}
