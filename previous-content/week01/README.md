# Week 01

## Data Structures

### Strings

Strings are simply characters, and I won't spend too much time going over them.

There are some things to know about how they operate.

#### Strings have indexes

```js
const testString = 'Hello World';
console.log(testString[0]);
// => returns 'H'
```

You can characters in a string by their index, which starts at 0, much like an array.

#### Strings are immutable

```js
let str = 'Hello';
str[0] = 'h';

console.log(str); // => 'Hello'
```

You cannot change the value, instead, you'd have to construct a new string and reassign it to the value.

### Booleans

You probably have a good grasp on booleans, but we should discuss _truthy_ and _falsy_ values which you can check against. Some of them are strange and unintuitive, such as infinity registering as _truthy_, but I digress. Here's a list of truthy values:

```js
if (true) {
  console.log('Truthy');
}

if ({}) {
  console.log('Truthy');
}

if ([]) {
  console.log('Truthy');
}

if (42) {
  console.log('Truthy');
}

if ('foo') {
  console.log('Truthy');
}

if (new Date()) {
  console.log('Truthy');
}

if (-42) {
  console.log('Truthy');
}

if (3.14) {
  console.log('Truthy');
}

if (-3.14) {
  console.log('Truthy');
}

if (Infinity) {
  console.log('Truthy');
}

if (-Infinity) {
  console.log('Truthy');
}
```

And the opposite:

```js
// These shouldn't print

if (false) {
  console.log('Falsy');
}

if (null) {
  console.log('Falsy');
}

if (undefined) {
  console.log('Falsy');
}

if (0) {
  console.log('Falsy');
}

if (NaN) {
  console.log('Falsy');
}

if ('') {
  console.log('Falsy');
}

if ('') {
  console.log('Falsy');
}

if (``) {
  console.log('Falsy');
}

if (document.all) {
  console.log('Falsy');
}
```

### Control Flows

I'm simply going to point you [here](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling), MDN has covered this much more thoroughly than I can write. The most common I see are `if` and `switch`, but it'll be good for you to use each of them, which will be covered in this week's challenges.

### Arrays and Sets

So, judging from your codepens, you're familiar with arrays. Simply, arrays are an indexed data structure that contains data. But arrays have a sister data structure called _Sets_.

```js
const exampleSet = new Set([1, 2, 3]);
const exampleArray = [1, 2, 3];
```

Sets do not have indexes, and each element contained within a set is unique.

```js
console.log(exampleSet[0]); // undefined;
console.log(exampleArray[0]); // 1

exampleSet.add(1);
exampleArray.push(1);

console.log(exampleSet); // [ 1, 2, 3 ]
console.log(exampleArray); // [1, 2, 3, 1]
```

Sets are really nice for deduping possible duplicate entries, because each entry must be unique. Sets are also _sometimes_ more efficient than arrays, for example, _prepending_ things to a set is faster, but these small optimizations are things you will rarely have to worry about. It's just good for you to know they exist.

### Objects and Classes

So, from the codepen's I've looked over, you have a grasp on strings, arrays, numbers, but not many examples of objects or classes.

These two structures allow us to construct sensible groupings of these values (which may include other objects) to build complex structures.

Let's imagine a dog show.

Every dog in the show has a name, a breed, and scores, just to keep it simple. What would this, if we wanted to represent it in an _object_, look like?

```js
{
  name: 'Chaser',
  breed: 'Border Collie',
  scores: [10, 10, 10, 10]
}
```

Now, we have an ambiguous structure that contains all the information for Chaser.

But, is this the best structure to use in this instance? Since we know consistently what a dog will be, and have a hypothetical blueprint of what a dog is and what it looks like, a class might be a better way of representing the contestants.

```js
class Dog {
  constructor(name, breed, scores) {
    this.name: name,
    this.breed: breed,
    this.scores: scores
  }
}
```

Now, we have a logical grouping of all this data, and what it might need to know. We can also expand on this structure, and start making useful methods for this class.

```js
class Dog {
  constructor(name, breed, scores) {
    this.name: name,
    this.breed: breed,
    this.scores: scores
  }

  averageScores() {
    const reducer = (accumulator, currentValue) => accumulator + currentValue;

    return this.scores.reduce(reducer) / this.scores.length
  }
}
```

This way, we can help keep our code organized and contained. Each class should only know, understand, and handle what it needs.

## Challenges

You'll find some challenges I've made [here](./challenges). In order to get the test suite working, you'll need to run a couple things in your terminal (ignore the dollar sign, it just signifies run it in your terminal).

```
$ npm install
```

Then, once installed, simply run:

```
$ npm test
```

You'll notice that the first one immediately breaks. There aren't many tests, given the simplicity of the challenges. You'll notice in the test file (signified with the `.test.js`) there is an 'xit', the 'x' prevents the test suite from running that particular test. When you get the first one working, remove those that 'x' to make the next one run.
