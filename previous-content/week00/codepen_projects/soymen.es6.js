// color scheme https://coolors.co/6699cc-fff275-ff8c42-ff3c38-a23e48
// freecodecamp codepen https://codepen.io/Em-Ant/full/QbRyqq/
// good design example https://codepen.io/apositivejam/pen/YyxbmL?editors=1100

//Each time a box is clicked, function will make the box brighter, then one second later return back to normal.
//If another box is clicked with less than a second, the first box will return back to normal immediately and do nothing else.

// ES6 allows us to use let, and const over variables.
// While var is still available, the one thing you should know is
// using var is *dangerous*.
// var's can be the source of major headaches for even seasoned
// developers.

// Here is a great stack overflow explanation:
// https://softwareengineering.stackexchange.com/questions/274342/is-there-any-reason-to-use-the-var-keyword-in-es6

let gameActive = false;
let strict = false;

// So, for this function is simple flipping the `strict` value,
// so instead of creating a function, we can simply call

// strict = !strict

// This will flip the boolean value, no need for a function.

// const toggleStrict = () => (strict ? (strict = false) : (strict = true));

const audio1 = new Audio('https://s3.amazonaws.com/freecodecamp/simonSound1.mp3');
const audio2 = new Audio('https://s3.amazonaws.com/freecodecamp/simonSound2.mp3');
const audio3 = new Audio('https://s3.amazonaws.com/freecodecamp/simonSound3.mp3');
const audio4 = new Audio('https://s3.amazonaws.com/freecodecamp/simonSound4.mp3');
const audios = [audio1, audio2, audio3, audio4];

let songIndex = 0;

let computerMovesArray = [];
let playerMovesArray = [];

// var numOfComputerMoves = function() {
//   return computerMovesArray.length;
// };

// So, thinking about these functions. They simply calls the length of
// these arrays, again, no need for a function to achieve this.
// Wherever this is called, we can simply call:

// computerMovesArray.length
// playerMovesArray.length

// If this value is called often, we can, instead of using a function,
// assign it to a variable that is locally scoped. Let's try and use as few
// global variables as possible.

// let numOfComputerMoves = computerMovesArray.length

// let numOfComputerMoves = () => computerMovesArray.length;
// let numOfPlayerMoves = () => playerMovesArray.length;

/*
In ES6, we use arrow functions. The number is the parameter, the arrow is the return.
Arrow functions have a nice implicit return when there is no block. 

But, if the function does more heavy lifting, we can also write a return block.

const playAudio = number => {
  const numberToPlay = number - 1;
  return audios[number - 1].play()
}

Arrow functions also lexically are bound to `this`, so we can avoid insane binding callbacks.
*/

// var playAudio = function(number) {
//   var index = number - 1;
//   return audios[index].play();
// };
const playAudio = number => audios[number - 1].play();

// var removeOpacity = function(spot) {
//   $("#soymen-button-" + spot).removeClass("add-opacity");
// };

// We can just name the function. I think it's easier to read because it differentiates
// a var from a function immediately at the first character. Also, we don't expect these
// namespaces to be reassigned, removeOpacity() will always be what it is declared as here.

function removeOpacity(spot) {
  $('#soymen-button-' + spot).removeClass('add-opacity');
}

function addOpacity(spot) {
  $('#soymen-button-' + spot).addClass('add-opacity');
  setTimeout(function() {
    removeOpacity(spot);
  }, 300);
}

function resetGame() {
  $(this).html('Reset Game');
  gameActive = false;

  // Splice is *destructive*.
  // Which is dangerous.

  // Imagine if you wanted to save the game states, so that players could replay their game up to a certain point.
  // When you use splice, you destroy that data structure, it no longer exists.
  // You should avoid destructive methods.

  // computerMovesArray.splice(0);
  // playerMovesArray.splice(0);

  // Instead, we should use [].slice(). Slice returns a *new* array, which we can set the value to be.
  // In practice, this ends up being the same, and it's hard to illustrate my point. But, imagine if you had
  // two arrays, one needed elemeents copied from the other:

  /*

  let originalArray = [0, 1, 2, 3];
  let copyArray = originalArray.splice(0, 1)

  console.log(originalArray);
  console.log(copyArray);
  */

  // You'll see the original array was changed as well. This leads to headaches.

  computerMovesArray = computerMovesArray.slice(0);
  playerMovesArray = playerMovesArray.slice(0);

  $('.soymen-status').html(computerMovesArray.length);
  console.clear();
  console.log('Ok, starting from scratch');
  playComputer();
}

function winnerFound() {
  gameActive = false;
  $('.soymen-status').html('Winner!');
}

function playPlayer(spot) {
  if (gameActive) {
    playAudio(spot);
    playerMovesArray.push(spot);
    addOpacity(spot);
    console.log('You just played at ' + playerMovesArray);
    for (var i = 0; i < numOfPlayerMoves(); i++) {
      if (playerMovesArray[i] !== computerMovesArray[i]) {
        $('.soymen-status').html('!!');
        playerMovesArray.splice(0);
        strict ? resetGame() : setTimeout(readComputerMoves, 1000);
      }
    }
  }
}

var playPlayer = function(spot) {
  return function() {
    if (gameActive) {
      playAudio(spot);

      // push() is another destructive method
      // playerMovesArray.push(spot);

      playerMovesArray = playerMovesArray.concat(spot);
      addOpacity(spot);
      console.log('You just played at ' + playerMovesArray);
      for (var i = 0; i < numOfPlayerMoves(); i++) {
        if (playerMovesArray[i] !== computerMovesArray[i]) {
          $('.soymen-status').html('!!');
          playerMovesArray.splice(0);
          strict ? resetGame() : setTimeout(readComputerMoves, 1000);
        }
      }
      if (numOfPlayerMoves === 20) {
        winnerFound();
      } else {
        if (numOfPlayerMoves() === numOfComputerMoves()) {
          playComputer();
        }
      }
    }
  };
};

for (var i = 1; i < 5; i++) {
  $('#soymen-button-' + i).click(playPlayer(i));
}

var generateComputerMove = function() {
  var randomNumber = Math.floor(Math.random() * 4 + 1);
  computerMovesArray.push(randomNumber);
  console.log('Computer played at ' + randomNumber);
};
var readComputerMoves = function() {
  $('.soymen-status').html(numOfComputerMoves());
  gameActive = false;
  playerMoves = 0;
  playerMovesArray.splice(0);
  var timeSounds = setInterval(function() {
    playAudio(computerMovesArray[songIndex]);
    addOpacity(computerMovesArray[songIndex]);
    if (songIndex < numOfComputerMoves() - 1) {
      songIndex++;
    } else {
      songIndex = 0;
      gameActive = true;
      clearInterval(timeSounds);
    }
  }, 1000);
};

var playComputer = function() {
  generateComputerMove();
  readComputerMoves();
};

$('#soymen-game-begin-reset').click(resetGame);

$('#soymen-game-start').click(function() {
  if (!gameActive) {
    playComputer();
  }
});

$('.soymen-strict-button').click(function() {
  toggleStrict();
  strict ? $(this).addClass('green-background') : $(this).removeClass('green-background');
});

$(document).ready(function() {
  $('#soymen-game-begin-reset').html('Begin game');
});
