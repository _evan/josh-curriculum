// color scheme https://coolors.co/6699cc-fff275-ff8c42-ff3c38-a23e48
// freecodecamp codepen https://codepen.io/Em-Ant/full/QbRyqq/
// good design example https://codepen.io/apositivejam/pen/YyxbmL?editors=1100

//Each time a box is clicked, function will make the box brighter, then one second later return back to normal.
//If another box is clicked with less than a second, the first box will return back to normal immediately and do nothing else.

var gameActive = false;
var strict = false;
var toggleStrict = function() {
  return strict ? (strict = false) : (strict = true);
};
var audio1 = new Audio("https://s3.amazonaws.com/freecodecamp/simonSound1.mp3");
var audio2 = new Audio("https://s3.amazonaws.com/freecodecamp/simonSound2.mp3");
var audio3 = new Audio("https://s3.amazonaws.com/freecodecamp/simonSound3.mp3");
var audio4 = new Audio("https://s3.amazonaws.com/freecodecamp/simonSound4.mp3");
var audios = [audio1, audio2, audio3, audio4];
var songIndex = 0;

var computerMovesArray = [];
var playerMovesArray = [];
var numOfComputerMoves = function() {
  return computerMovesArray.length;
};
var numOfPlayerMoves = function() {
  return playerMovesArray.length;
};

var playAudio = function(number) {
  var index = number - 1;
  return audios[index].play();
};

var removeOpacity = function(spot) {
  $("#soymen-button-" + spot).removeClass("add-opacity");
};

var addOpacity = function(spot) {
  $("#soymen-button-" + spot).addClass("add-opacity");
  setTimeout(function() {
    removeOpacity(spot);
  }, 300);
};

var resetGame = function() {
  $(this).html("Reset Game");
  gameActive = false;
  computerMovesArray.splice(0);
  playerMovesArray.splice(0);
  $(".soymen-status").html(numOfComputerMoves());
  console.clear();
  console.log("Ok, starting from scratch");
  playComputer();
};

var winnerFound = function() {
  gameActive = false;
  $(".soymen-status").html("Winner!");
};

var playPlayer = function(spot) {
  return function() {
    if (gameActive) {
      playAudio(spot);
      playerMovesArray.push(spot);
      addOpacity(spot);
      console.log("You just played at " + playerMovesArray);
      for (var i = 0; i < numOfPlayerMoves(); i++) {
        if (playerMovesArray[i] !== computerMovesArray[i]) {
          $(".soymen-status").html("!!");
          playerMovesArray.splice(0);
          strict ? resetGame() : setTimeout(readComputerMoves, 1000);
        }
      }
      if (numOfPlayerMoves === 20) {
        winnerFound();
      } else {
        if (numOfPlayerMoves() === numOfComputerMoves()) {
          playComputer();
        }
      }
    }
  };
};

for (var i = 1; i < 5; i++) {
  $("#soymen-button-" + i).click(playPlayer(i));
}

var generateComputerMove = function() {
  var randomNumber = Math.floor(Math.random() * 4 + 1);
  computerMovesArray.push(randomNumber);
  console.log("Computer played at " + randomNumber);
};
var readComputerMoves = function() {
  $(".soymen-status").html(numOfComputerMoves());
  gameActive = false;
  playerMoves = 0;
  playerMovesArray.splice(0);
  var timeSounds = setInterval(function() {
    playAudio(computerMovesArray[songIndex]);
    addOpacity(computerMovesArray[songIndex]);
    if (songIndex < numOfComputerMoves() - 1) {
      songIndex++;
    } else {
      songIndex = 0;
      gameActive = true;
      clearInterval(timeSounds);
    }
  }, 1000);
};

var playComputer = function() {
  generateComputerMove();
  readComputerMoves();
};

$("#soymen-game-begin-reset").click(resetGame);

$("#soymen-game-start").click(function() {
  if (!gameActive) {
    playComputer();
  }
});

$(".soymen-strict-button").click(function() {
  toggleStrict();
  strict
    ? $(this).addClass("green-background")
    : $(this).removeClass("green-background");
});

$(document).ready(function() {
  $("#soymen-game-begin-reset").html("Begin game");
});
