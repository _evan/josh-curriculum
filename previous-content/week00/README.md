# Week One

## Agenda

- Get acquainted with the command line
- Setup Development Environment
  - brew
  - git
  - node
  - VScode
- Transitioning to ES6
- Reviewing Codepen Work

## Get acquainted with the command line

Before anything, we have to make sure you're comfortable working in your terminal. No developer can get away from it, everything a developer does on their day to day have no graphical interface, and depend on text based systems to navigate, test, and debug.

A terminal is an interface to a shell. A shell is what interprets your commands, and executes on them. This is true for all Unix machines, but we don't have to get into the specifics.

You will have to learn some commands, and you should understand what your computer is doing when you type in these commands, and learn how to use them.

When you type in `ls` in your terminal, your shell executes the binary located in `/bin/ls`, which is where the code for that command is stored. It's written in binary, so no need to mess around in there. Your terminal understands that the binaries in this folder, or `directory`, or `dir` as you might also see, are executed. I don't want to get too deep into this, again, it's a whole world, but here are some basic commands everyone uses:

- `cd`
- `ls`
- `grep`
- `man`
- `cat`
- `rm`
- `echo`

To read on how to use these commands, you can type into your terminal `man ____`, with the blank being the command.

Please go through the [codecademy](https://www.codecademy.com/learn/learn-the-command-line) lessons 1-3, they have a good interactive tutorial. We'll get back to lessons 4-5 next week.

## Setup Development Environment

Now you're acquainted, let's start installing some of our own tools outside of the default unix ones.

## Brew

Brew is an unofficial package manager for MacOS. Package managers track versions of things you install, keeps track on if they're up to date, and removes old versions as you update packages. Most unixes have their own official package manager, but Apple does not, so we have to install one.

Copy and paste this into your termninal:

`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

Wait for it to finish installing, and now when you type `brew`, it should return something like this:

![Brew Snapshot](../assets/week01_brew.png?raw=true 'Brew Snapshot')

## Git

Run this command in your terminal:

`brew install git`

Then verify it was installed, when you enter `git --version`, it should return something similar to:

`git version 2.9.2`

### Resources

[learngitcommit](https://learngitbranching.js.org/) is a great place to visualize how git works.

[Getting git right](https://www.atlassian.com/git) from Atlassian is a good resource, the first section labeled **Learn Git** is basically a salespitch for bit bucket, so I'd say don't waste time.

[Git for Developers](https://www.atlassian.com/git/tutorials/why-git#git-for-developers) is a good section on the problem git was made to solve, and the rest in that chapter is not relevant.

[Getting Started](https://www.atlassian.com/git/tutorials/setting-up-a-repository), get started working down this article.

- Please finish both **Getting Started** and **Collaborating**
- Skip **Migrating to Git**
- Optional: **Advanced Tips** has good stuff in there, things you'll eventually need. Up to you.

## Node/npm

We'll talk about Node/npm and the JavaScript environment later, for now, please run these commands:

`brew install node`

Which will install both `node` and `npm`.

## Editor

We should install an editor. For JavaScript, I like [VS Code](https://code.visualstudio.com/docs/setup/mac#_installation), it has a lot of nice integrations specifically around JavaScript development, but this is a personal choice, whichever you can work the fastest in, go ahead and install.

## Transitioning to ES6

When I was reviewing your codepens, I noticed your code was looking a little dated. Unfortunately, freecodecamp haven't been keeping up to date with modern JavaScript development.

We should begin talking about ES6, the newest standard for JavaScript development. It's not just symantics, it's important the JavaScript community remains cohesive and consistent, otherwise the development world becomes splintered and chaotic.

Let's begin working through transitioning those codepens to the newest standards.

TODO: Finish Transition Section

## Challenge

In this directory, I've added a [text file](./challenge00-1.txt). Please follow the instructions inside, commit it, and push it up in a separate branch.

## Additional Reading

Writing Good Commit Messages:

https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
