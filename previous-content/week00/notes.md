Date: 12/25/18


## QUESTIONS:
- Recap of Git workflow
- What is a "release" and why do we use it?
- What is a forward merge?
    - Ensuring you are up to date with most recent changes in the repo
- What is rebasing?
    - consolidating commits from a whole chapter into just 1 commit
        - PROS - much cleaner codebase, easier to follow / read
        - CONS - Difficult to go back to a specific commit and track when something took place 
- Is it common to integrate task tracking project management software with git?
    - Yes, but with a Git platform
        - Common with issues
        - Same with features


## NOTES:

### RELEASE PROCESS - 1
MASTER
- Beta 
    - Feature 1
    - Feature 2
    - Feature 3

MERGE BETA > MASTER

### RELEASE PROCESS - 2

MASTER
- Feature 1 > MERGE MASTER
- Feature 2 > MERGE MASTER
- Feature 3 > MERGE MASTER

More Agile approach, as the more time that progresses, the more it will decay.

Pull requests are still implemented, 2nd reviewer needs to merge. 


## ADDITIONAL READINGS:
- [Getting Rid of JQuery](http://youmightnotneedjquery.com/)
- [Versions](https://semver.org/)
