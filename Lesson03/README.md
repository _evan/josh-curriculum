# Lesson 3

### Prerequisite:
Make a free account on [Heroku](https://heroku.com)

### To read beforehand:
**Intro to Node.js**: [https://medium.freecodecamp.org/what-exactly-is-node-js-ae36e97449f5](https://medium.freecodecamp.org/what-exactly-is-node-js-ae36e97449f5)

**A page about popularity of Node.js frameworks**: [https://2018.stateofjs.com/back-end-frameworks/overview/](https://2018.stateofjs.com/back-end-frameworks/overview/)

### Node.js

Node.js has gained a lot of traction in recent years, and is now used in some way by most tech companies.

**Pros**:

- easy to use, developer friendly
- flexible
- integrates with NPM and huge ecosystem
- Javascript's event-driven model is a good way to think about backend processes also

**Cons**:

- Javascript does not "true concurrency". Node's "event loop" model makes it seems like many things are happening at the same time, but in reality Node is completing operations sequetially... meaning that it is limited in speed/scalability.

**Differences with JS**:

- instead of `import` we will use `require` to import code

### Express.js

Express is the most popular Node.js framework for creating web servers. It is based on years of learnings from other user-friendly backend frameworks in Ruby, Python, and PHP. It has tools for accomplishing common tasks that backend developers face:

- REST APIs
- Routing
- Middleware (functions that run between the web request and the web response)
- Hooking up to databases
- Templating (we will not use this, as React is basically a frontend templating engine)

*Here is some standard boilerplay Express.js code that I start all my server projects with:*
```js
// index.js
const express = require('express')
const bodyParser = require('body-parser') // for parsing bodies of POST requests
const cors = require('cors') // Cross-Origin Resource Sharing
const helmet = require('helmet') // for security

// create the app and add some middleware
const app = express()
app.use(helmet())
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.disable('x-powered-by') // for security

// Heroku will provide you with a PORT in your Environment
const port = process.env.PORT || 5000
app.listen(port, function() {
  console.log("Listening on port " + port + "!") // start the app!
})
```

*And a package.json starting point:*
```json
{
  "main": "index.js",
  "scripts": {
    "start": "node --inspect index.js",
  },
  "dependencies": {
    "body-parser": "^1.18.3",
    "cors": "^2.8.4",
    "express": "^4.15.4",
    "helmet": "^3.13.0",
    "pg": "^7.4.3"
  }
}
```

### Project architecture

- `index.js`: put the above code in here
- `package.json`
- `.gitignore`: add `node_modules` to this file
- `Procfile`: this is the file that Heroku uses to start our app. Add the content `web: node index.js`

To start the app:
```bash
npm install
node index.js
# will output "Listening on port 5000!"
```


### Creating and app and database

```bash
#heroku depends on git to work, so initialized a git repo first
git init

#download heroku-cli
brew tap heroku/brew && brew install heroku

#create your app
heroku create modelwiz-server
```

Then in the Heroku online dashboard, go to your apps, and go to Resources. Then search for the Postgres addon, and add it to your app. Now you have a free database!

```bash
#see the URL of your Postgres DB, which you can use to connect to it
heroku config

#connect to your DB from the command line
heroku pg:psql
```

The first thing we will need to do is create a new table
```sql
CREATE TABLE contacts (
  id bigserial NOT NULL primary key,
  email TEXT
);
```

We will use the "pg" library for Node to connect to our Postgres DB. Make a new file called `db.js` and put this content in it:
```js
const { Client, Pool } = require('pg')

const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
  ssl: true,
})

module.exports = {
  createContact: async (email) => pool.query(`INSERT INTO contacts(email) VALUES($1)`, [email])
}
```

A couple notes about the above code:

- `module.exports` is how you export code from a file in Node (instead of `export` in ES6)
- We have defined the `createContact` function as an `async` function, meaning that you can `await` the function (more on this later)
- We are getting the DB url out of the environment (in Heroku it is provided for you)
- To add variables to your local envionment, you can use `export DATABASE_URL=***`


### Routes in Express

Now we can add our first route to our Express app! Routes are urls that come in four different flavors:

- GET: for retrieving resources
- POST: for creating resources
- PUT: for editing resources
- DELETE: for deleting resource

A route to save the emails of people that sign up will be a POST route, becuase we are creating a new record in the DB

```js
// import the db file at the top of index.js
const db = require('./db')

// note this is a "post" route
app.post('/contact', async (req, res, next) => {
  try {
    const user = await db.createContact(req.body.email)
    return res.status(200).json({user})
  } catch(e) {
    return res.status(500).json('Failed to create contact')
  }
})
```

Now we have a working route to create records of contacts who have signed up for email updates! To deploy:
```bash
heroku git:remote -a modelwiz-server
git add .
git commit -m "first commit"
git push heroku master
```
