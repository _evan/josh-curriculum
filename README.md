# Curriculum

- [Lesson01](https://bitbucket.org/_evan/josh-curriculum/src/master/Lesson01): website deployment and architecture
- [Lesson02](https://bitbucket.org/_evan/josh-curriculum/src/master/Lesson02): finish deployment, getting started with React.js, ES6
- [Lesson03](https://bitbucket.org/_evan/josh-curriculum/src/master/Lesson03): Node.js, Heroku, Postgres
- [Lesson04](https://bitbucket.org/_evan/josh-curriculum/src/master/Lesson04): connecting the frontend to the backend
- [Lesson05](https://bitbucket.org/_evan/josh-curriculum/src/master/Lesson05): javascript syntax (curly braces)
- [Lesson06](https://bitbucket.org/_evan/josh-curriculum/src/master/Lesson06): secure authentication (JWT, bcrypt)
