
# Lesson 2

### To read beforehand:
**Intro to JSX Syntax**: [https://reactjs.org/docs/introducing-jsx.html](https://reactjs.org/docs/introducing-jsx.html)
**Intro to ES6 and JSX**: [https://www.fullstackreact.com/30-days-of-react/day-2/](https://www.fullstackreact.com/30-days-of-react/day-2/), an intro to JSX and ES6 Javascript, shows you how JSX is really just Javascript under the hood.

### Finish up deployment stuff!

GoDaddy is a little more complicated than some other domain services I have used, but I figured out how to make it point to zeit.co. On GoDaddy's DNS page:

- CNAME record from `www.modelwiz.com` to `alias.zeit.co` (we already this)
- Forwarding to `http://www.modelwiz.com` (we already did this)
- A record from `@` to `50.63.202.1` (GoDaddy's forwarding service)

On zeit.co:

- Create a **certificate** for `www.modelwiz.com`. You will see a "challenge record" that you will have to add as a TXT record to GoDaddy DNS
- Create an **alias** for `www.modelwiz.com`
- Add that alias to the actual deployment of the site
- Now when someone visits `modelwiz.com`, it will redirect to `www.modelwiz.com` which will show your deployed site from zeit.co (with HTTPS!!!)


### Notes on release process
Every time you deploy your site with `now`, it creates a new copy of your site at a new domain (with a random string in the domain name). You can test the site there to make sure there are not bugs. This testing phase becomes more relevant when you are hooked up to a backend as well, so you can make sure that you didn't accidentally introduce a bug in the way that the frontend connects to the backend. When you are ready to "upgrade" the newest deployment to production, you just switch the zeit.co **alias** to the new deployment!

Backend release process is similar. When we start using Heroku, I will show you how to "upgrade" a deployment to production.

### Getting started with React using create-react-app
Modern web developers heavily rely on NPM packages to provide functionality and components in their app. There is one issue with this: NPM packages often depend on *other* NPM packages, and if you import one, you are also importing all of its dependencies. This can lead to a huge mess of dependencies, many of them duplicated or not even used. For this reason, modern web developers use **bundlers** to automatically sift through their dependencies and optimize the final code that is exported (this is called "tree-shaking"). Bundlers can also transpile future versions of Javascript (ES6, ES7, etc) down to code that every browser can understand and render. This enables developers to use features of Javascript that aren't even released yet!!!

**Webpack**
The most popular bundles is [**webpack**](https://webpack.js.org/). It is super powerful, BUT it is a huge hassle to get set up, and it requires a lot of knowledge to customize it.

**create-react-app**
Last year, Facebook released a CLI (command line interface) called **create-react-app** that includes a pre-defined webpack build step, so you don't have to understand webpack at all! This tool has been massively popular, and most people starting a React project use it. It also includes a "dev server" with cool features like automatic reloading of your application in the browser whenever you make a change to your code.
```bash
# to install create-react-app (the -g stands for "global")
npm install -g create-react-app

# then navigate to the directory where you want to make your new app
# and run the following command to make a new app in a "modelwiz" directory
create-react-app modelwiz

# navigate to that directory, and start your "dev server" like this:
npm start

# a message will pop up asking you to grant NPM access to your browser,
# after you click "yes", a simple appliation will pop open in your browser!
```

### React project architecture
You will see a few files and folders in your project:

- **package.json**: This important file keeps track of your project dependencies
- **node_modules/** this folder actually stores those dependencies
- **public/** this folder stores your index.html file, and other assets like images
- **src/** this is where your actual app code lives!
- **.gitignore** this file tells git to only store the key files that make up your app. Most importantly, it ignores the node_modules folder, which could be many megabytes in size. When someone clones your app, they can just run `npm install` to re-install the node_modules folder (based on what package.json contains)

Open the **src/** folder in your editor and lets check out what React code looks like!

### Intro to a few ES6 features of Javascript

- **import/export**: These ES6 keywords allow you to share code between different .js files. Normally, if you were to create a variable like this `const hello = 'hello'`, it would be available at the "global scope". But now with import/export, the variables you create in a file only belong to that file, and you can choose which variables to share by exporting them. Why is the so important? Because now you can import other people's code, and you don't have to worry that their variable names might conflict with yours.

- **=>**: fat arrow syntax! This is a new way of making functions. 

Before, you could make a function like this:
`var f = function(){ console.log('hello') }`

now you can make a function like this:
`const f = () => console.log('hello')`

This difference is subtle, but very important. In Javascript there is a special keyword called "this", which refers to the "scope" of a function. For example, if a function is called from clicking a button, inside the function "this" will refer to the button. However, using the => syntax, the scope of the function always becomes the code where it was defined. This is very useful for creating components and classes.

- **class**: 
```javascript
class Example {
  n = 1
  count = () => {
    this.n ++
    console.log(this.n)
  }
}

const c = new Example

c.count()
```

### React syntax

**JSX**: a hybrid of Javascript and HTML, invented by Facebook. It is the easiest way to write React applications, and let you create very clean code.
```jsx
class App extends Component {
  render() {
    return (
      <div className="App">
        <header style={{background:'black'}}>
          <p onClick={()=> console.log('clicked!')}>
            This is what JSX code looks like
          </p>
        </header>
      </div>
    );
  }
}
```

Standard terminology:

- **Element**: same as in HTML, an element is like `<div />`
- **Attribute**: you add these to elements to change them, like `className`
- **Object**: a regular Javascript data type, enclosed by curly braces `{}`
- **Property**: the properties of objects. `{name:'Evan'}`. The name of a property does not have to be surrounded by quotes, but the actual value needs to be a valid data type (string, number, boolean, or function).

Notes on React JSX syntax:

- since `class` is now a Javascript keyword, React uses `className` instead to designate CSS classes
- element attributes are written in "camelCase", like `onClick` or `className` (the first letter is lowercase, and then the first letter of each new word is uppercase)
- attribute values are objects `{}` instead of strings like they are in regular HTML. The `style` attribute is another object with properties for each rule, that is why you see double curly braces `{{}}` for `style`

### Let's start customizing our App!

Paste this content into App.js and App.css to get a start on customizing your app. Also paste the hero.jpg background image into /src

```jsx
import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>

        <header className="app-header">
          <h1>ModelWiz</h1>
          <div className="app-menu">
            <a>Menu Item 1</a>
            <a>Menu Item 2</a>
            <a>Menu Item 3</a>
          </div>
        </header>

        <section className="section-1">
          <h1>Financial Modeling Made Easy!</h1>
        </section>

      </div>
    );
  }
}

export default App;
```

```css
body{
  color:white;
}
.app-header {
  background-color: #282c34;
  min-height: 70px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding:0 32px;
  position:fixed;
  top:0;
  width:calc(100% - 64px);
}
.app-menu a{
  margin:0 16px;
  color:#DDDDDD;
  cursor:pointer;
}
.app-menu a:hover{
  color:white;
  text-decoration:underline;
}
.section-1 {
  height:100vh;
  background: linear-gradient(to bottom, rgba(92, 77, 66, 0.3) 0%, rgba(20, 77, 99, 0.8) 100%), url(./hero.jpg);
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
}
.section-1 h1{
  font-size: 3rem;
}
```

# Pages and Routing

Now we will install our first React component! `react-router-dom`. Hopefully now you will see why React is so amazing! Because you can easily integrate components that other people have written into your applications

`npm install --save react-router-dom`

`npm start`

```js
// import the following components at the top of App.js
import { BrowserRouter, Route, Link } from 'react-router-dom'
```

```jsx
// wrap your top-level div in the <BrowserRouter> component
return (
  <BrowserRouter>
    <div>
      ...
    </div>
  </BrowserRouter>
);
```

```jsx
// instead of the "<a>" tags, use <Link> components for your menu items
<Link to="/about">About</Link>
```

```jsx
// now you can define routes for each page!!!
<Route exact path='/' component={()=>(
  <section className="section-1">
    <h1>Financial Modeling Made Easy!</h1>
  </section>)
}/>
```

# Homework

Add some content to the sections, and style them how you would like to. You can always add a new section as well! Next week we will learn about:

- How to create our own component for each page, and split them out into their own files.
- Building and deploying React apps (create-react-app makes this very easy)
- How to work with the React's `state` feature