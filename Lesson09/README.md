### working with excel files in Node or browser

- Here is the library for working with Excel in javascript: https://github.com/exceljs/exceljs
- Integrations with a web app:
    - Download from OneDrive? Sharepoint? Or just flat file uploads? 
    - Real-time updating documents would be crazy complicated
    - You could generate Excel sheets in the browser too
    - Whats the workflow?


```js
const Excel = require('exceljs')
const fetch = require('node-fetch')
const filename = 'Book.xlsx'

// read from a file
var workbook = new Excel.Workbook();

async function readFile() {
  await workbook.xlsx.readFile(filename)

  workbook.eachSheet(function (worksheet, sheetId) {

    worksheet.eachRow({ includeEmpty: true }, (row, rowNumber) => {

      let rowContent = ''
      row.eachCell({ includeEmpty: false }, (cell, cellNumber) => {
        if (cellNumber !== 1) rowContent += ','
        rowContent += cell.value
      });
      console.log(`Row ${rowNumber}:`, rowContent)

    });

  });

}

readFile()
```

### Managing React State

Sometimes it gets really messy passing everything down as props. What if you want to share a piece of state between components that are far away from each other in component hierarchy?? There are many options to solve this issue. The most popular has been Redux, and I like another one called Unistore. However, React recently added this feature into the React library itself, called "context".

We can use this to hide pages if the user is not logged in

AppContext.js
```js
import React from 'react'
export const AppContext = React.createContext()
```

In your top-level App.js:
```js
// import your context
import {AppContext} from './AppContext'

// add a constructor with some state that we want to be "global"
constructor(){
  super()
  this.state={
    user:{},
  }
}

// Wrap your entire component in a context provider
<AppContext.Provider value={{
  store:this.state,
  setUser:(user)=>this.setState({user}),
}}>
  ...
</AppContext.Provider>
```

In your login component:
```js
// import here also
import {AppContext} from './AppContext'

// at very bottom (before exporting):
Login.contextType = AppContext
```
