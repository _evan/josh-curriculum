# Lesson05

JS syntax review

### Syntax Review

I think its a good idea to do a 10-15 minute syntax review at the beginning of each session... Since I have been using JS for long its easy for me to forget how funky and complex Javascript syntax can be, especially for beginners. I will share my screen and just talk over some weird parts of Javascript. Today we will tackle the weirdest part of all, the curly braces `{}`

**Curly Braces have many uses**

They are very important in function declarations:
```js
// regular function declaration
function hi() { /*...*/ }

// named function declaration
const hi = function() { /*..*/ }

// fat arrow declaration!
const hi = () => { /*...*/ }
```

But they are also used JS **objects**. Objects have "properties" and "values". The properties are always strings, but you dont have to put them in quotes. Values can have many types. Each property is followed by a comma (except the last one)
```js
const obj = {
    hi: 'hello',        // string property
    number: 3,          // number property
    yes: true,          // boolean property
    func: function(){   // function property
        // code here
    },
    arr: [1,2,3],       // array property
    sub: {              // object property
        hi: 'hello'
    }
}

// of course, values can also be references to variables:
const hi = 'hello'
const obj = {
    hi: hi        // the first "hi" is the property name, the second one = "hello"
}

// with ES6, you can also write the above in a shorthand way:
const obj = {hi}
```

There is also another use for `{}`! It is a new feature of Javascript called "destructuring". It means "taking properties out of an object". So for example, lets say we have a component with `state={hi:'hello'}`. What if we wanted to put the value of `this.state.hi` into its own variable?
```js
// we could do this:
const hi = this.state.hi

// but we can also do this!
const {hi} = this.state
// we should get used to doing it this way, it makes our code cleaner
```

**Quick Quiz**:
```js
const hi = 'hello there!'
function hello(a){ console.log(a) }

// do these curly braces signify a function or an object?
// what do you think will actually print out when we run this?
hello({hi}) 

// what will this print?
const foo = (arg) => { console.log(arg) }
foo({arg:"blah blah"})

// what will this print?
const bar = ({arg}) => { console.log(arg) }
bar({arg:"blah blah"})

// What is the difference between the following?
<MyComponent attr={'hello'} />
<MyComponent attr="hello" />
```

Curly braces can be tricky, the quick quiz has some intentionally difficult questions in it... so don't worry if its frustrating! We will keep reviewing this stuff for sure. The more you write Javascript, the clearer it will become.

