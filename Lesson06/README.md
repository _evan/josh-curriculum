# Lesson06

Authentication!!! It might take 2 sessions to get through everything with user authentication, it can be pretty complicated.

### Readings

- Today we will start to build user authentication in Node.js. We will use JSON Web Tokens (JWT), here is a quick introduction: [https://auth0.com/docs/jwt](https://auth0.com/docs/jwt)
- Storing user passwords in the Postgres database can be a very complex subject, because it involves encryption algorithms. Good thing I have already researched this topic extensively and know the moden best practices! The algorithm we will use for storing user passwords on the server is called `bcrypt`, here is an article about it: [https://medium.com/@danboterhoven/why-you-should-use-bcrypt-to-hash-passwords-af330100b861](https://medium.com/@danboterhoven/why-you-should-use-bcrypt-to-hash-passwords-af330100b861). If it feels like you are jumping into the deep end with this stuff, its because you are! But I still think its best to do things the right way the first time. Encryption is really complicated and the truth is you dont need to know how it works, just which tools to use and how to use them.
- Specifically we will be using this library: [https://www.npmjs.com/package/bcrypt-nodejs](https://www.npmjs.com/package/bcrypt-nodejs)

### Validation and showing notifications

Lets finish up our frontend form! Blueprint has a nice little notification component that we can use to show users if the form submission was successful or not.

So for example we can show an error if the email is not valid:
```js
// here is a function to validate email using a regex (Regular Expression)
// you dont need to understand what this actually means! I certainly dont
function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
// and here is the code to initialize Blueprint's notification component
// remember to add "Toaster" to the components you are importing from Blueprint
export const Alerts = Toaster.create({
  position: 'top',
})
```

Then, inside your `onSubmit` function, you can show error or success messages like this:
```js
Alerts.show({
  message: "Error creating contact",
  intent: "danger",
})
```

### Making the frontend form for user authentication

Hopefully we have time to get this far! The form will pretty much use the same code at the "Coming soon" form... but maybe we should have some tabs for "Sign Up" and "Login".

```js
import { Tab, Tabs, Card } from "@blueprintjs/core";

<Card elevation={3}>
    <Tabs id="AuthTabs" onChange={this.handleTabChange} selectedTabId={selectedTabId}
    large={true} className="auth-tabs">
    <Tab id="login" title="Log In" panel={<LogInPanel/>} />
    <Tab id="signup" title="Sign Up" panel={<SignupPanel/>} />
    </Tabs>
</Card>
```

Here's a CSS thing to add...
```css
.auth-tabs .bp3-tab{
  outline:none;
  width:calc(50% - 10px);
}
.auth-tabs .bp3-tab-indicator-wrapper{
  width:calc(50% - 10px) !important;
}
```

### Deploying React to Zeit

If we have time, we can try to build and deploy our application to zeit. This is made very easy by `now` (once we have it set up). The following configurations I got from [https://teamtreehouse.com/library/deploy-to-now](https://teamtreehouse.com/library/deploy-to-now)

now.json:
```json
{
  "version": 2,
  "name": "modelwiz",
  "builds": [
    {
      "src": "package.json", 
      "use": "@now/static-build", 
      "config": {
        "useBuildUtils": "@now/build-utils@canary",
      }
    }
  ],
  "routes": [
    {"src": "^/static/(.*)", "dest": "/static/$1"},
    {"src": "^/favicon.png", "dest": "/favicon.png"},
    {"src": "^/asset-manifest.json", "dest": "/asset-manifest.json"},
    {"src": "^/manifest.json", "dest": "/manifest.json"},
    {"src": "^/service-worker.js", "headers": {"cache-control": "s-maxage=0"}, "dest": "/service-worker.js"},
    {"src": "^/precache-manifest.(.*)", "dest": "/precache-manifest.$1"},
    {"src": "^/(.*)", "dest": "/index.html"}
  ]
}
```

in package.json, we need to add the `now-build` script as well:
```json
{
  "scripts": {
    "now-build": "react-scripts build && mv build dist"
  }
}
```


### Homework

- We made a signup route today... do you think you can make the login route?
  - POST the username/password to Node
  - get the user by their username
  - compare the encrypted_password with the password that they submitted (using bcrypt.compareSync();)
  - if it is a match, make a JWT and return it to them!



